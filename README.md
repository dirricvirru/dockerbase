# Install Docker
If you don't have docker and docker-compose installed.  Follow the directions here: https://docs.docker.com/compose/install/

# Instructions for Using Docker Files in a Structyr Based Project

1.    Copy the docker-compose.yml and the .docker folder to a new or existing project
2.    Update the docker-compose.yml with project specific settings.  Follow the directions in the comments.
      This file is responsible for starting:
	  *    MySQL server instance
	  *    PhpMyAdmin
	  *    Php Project
3.    Update the .docker/Dockerfile with solution specific settings. Follow the directions in the comments.
      This file is responsible for setting up the PHP specific settings for compilation and hosting.  This file creates a linux instance with apache and has a starter vhost file.
4.    From command line or and IDE of your choice start the images.
    docker-compose up
	
Success Example:	
```
``>Successfully built 9aac9923e438``
``>Successfully tagged kcpoapp:latest``
``>Creating cme-customer-portal_app_1        ... done``
``>Creating cme-customer-portal_db_1  ... done``
``>Creating cme-customer-portal_phpmyadmin_1 ... done``
```
5.     Launch a terminal to your docker image.
6.     Navigate to the /var/www/html/#Project folder# and execute the commands.
*    composer install
*    npm install
    Let these commands finish, they take a while
    docker container attach #image name from .docker/Dockerfile#
7.    From a database dump from the staging environment, import the databases needed for the solution.
      *    **project**_master - the database required by structyr.
	  *    **project**_dev - This is the instance specific database for solution implementation and data.
	  
	  
8.    Inside the project Update your .env file.  See the sample.env file for notes on the changes you need to make.

9.    Update your hosts file C:\Windows\System32\drivers\etc\hosts
    127.0.0.1       dev.\[project\] #replace with your project setup#
    127.0.0.1       manager.dev.\[project\] #replace with your project setup#

10. Navigate to http://manager.dev.\[project\]  give it a minute to load.
If the default user is dev@somethingdomain try to use password n64j^!qIvtxhQT
